import datetime
import json
import os

import pydeezloader
import requests
from aiogram import Bot
from aiogram import types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor

# login telegram api
token = ''
if not token:
    with open('api_data.txt', 'r')as token:
        token = token.read()
        if not token == '':
            token = token
        else:
            with open('api_data.txt', 'w') as write:
                write.write(str(input('Пожалуйста, введите токен для бота -> ')))
            with open('api_data.txt', 'r')as token:
                token = token.read()

                if not token == '':
                    token = token
                else:
                    exit('Не указан токен бота.')

# login deezer api
arl_token = ''
if not arl_token:
    with open('d3d3z3r.txt', 'r') as arl_token:
        arl_token = arl_token.read()
        if not arl_token == '':
            arl_token = arl_token
        else:
            with open('d3d3z3r.txt', 'w') as write:
                write.write(str(input('Пожалуйста, введите arl токен -> ')))
            with open('d3d3z3r.txt', 'r')as arl_token:
                arl_token = arl_token.read()

                if not arl_token == '':
                    arl_token = arl_token
                else:
                    exit('Не указан arl.')

bot = Bot(token=token, parse_mode='HTML', validate_token=True)
dp = Dispatcher(bot=bot, storage=MemoryStorage())

download = pydeezloader.Login(arl_token)
api_deezer_url = 'https://api.deezer.com/'


class methods():
    def by_upc(upc: str = None) -> str:
        get_url = api_deezer_url + 'album/upc:' + str(upc)
        get_respone = requests.get(get_url).text
        return json.loads(get_respone)

    def get_tracklist(id: str = None) -> str:
        get_url = api_deezer_url + 'album/' + str(id) + '/tracks'
        get_respone = requests.get(get_url).text
        return json.loads(get_respone)

    def search(q: str = None) -> str:
        get_url = api_deezer_url + 'search/?q=' + str(q)
        get_respone = requests.get(get_url).text
        return json.loads(get_respone)


vers = '1.1'


@dp.message_handler(content_types=types.ContentTypes.TEXT, chat_type=types.ChatType.PRIVATE, commands='start')
async def any(msg: types.Message):
    await msg.answer(f'<i>ℹ  ️Версия: {vers}\n\n'
                     f'Применры поиска:\n'
                     f'- <code>3616551710212</>\n'
                     f'- <code>Lil Pump</></>')


@dp.message_handler(content_types=types.ContentTypes.TEXT, chat_type=types.ChatType.PRIVATE, regexp='\d')
async def any(msg: types.Message):
    get_data = methods.by_upc(upc=msg.text)
    if get_data == {'error': {'type': 'DataException', 'message': 'no data', 'code': 800}}:
        await msg.answer('<i>Код ошибки: </> <code>800</>\n'
                         '<i>Ответ от сервера:</> <code>No data</>\n\n'
                         f'<i>Ваш запрос: </><code>upc:{msg.text}</>')
    else:
        get_data = json.dumps(get_data)
        get_data = json.loads(get_data)

        album_id = get_data['id']
        album_title = get_data['title']
        album_link = get_data['link']
        album_cover = get_data['cover_xl']
        label = get_data['label']
        number_of_playlist = get_data['nb_tracks']
        duration_mins = str(datetime.timedelta(seconds=int(get_data['duration'])))
        release_date = get_data['release_date']
        artists = ' & '.join([artist['name'] for artist in get_data['contributors']])

        # получаю треклист и форматирую его

        tracklist_kb = types.InlineKeyboardMarkup()
        get_tracklist = methods.get_tracklist(album_id)
        get_tracklist = json.dumps(get_tracklist)
        get_tracklist = json.loads(get_tracklist)
        for track in get_tracklist['data']:
            artist = track['artist']['name']
            title = track['title']
            isrc = track['isrc']

            tracklist_kb.add(
                types.InlineKeyboardButton(text=f'{artist} - {title}', callback_data=f'{isrc}')
            )
        tracklist_kb.add(
            types.InlineKeyboardButton(text='Deezer', url=album_link)
        )

        # отправляю результат

        await msg.answer_photo(
            photo=requests.get(album_cover).content,
            caption=f'<i>Название: <code>{artists} - {album_title}</>\n'
                    f'Количество треков: <code>{str(number_of_playlist)}</>\n'
                    f'Длительность: <code>{duration_mins} минут</>\n'
                    f'Лейбл: <code>{label}</>\n'
                    f'Дата релиза: <code>{str(release_date)}</></>',
            reply_markup=tracklist_kb
        )


@dp.message_handler(content_types=types.ContentTypes.TEXT, chat_type=types.ChatType.PRIVATE, regexp='^[a-z]')
async def any(msg: types.Message):
    try:
        search = methods.search(msg.text)
        search = json.dumps(search)
        search = json.loads(search)
        kb = types.InlineKeyboardMarkup()
        index = []
        for result in search['data']:
            title = result['title']
            artist = result['artist']['name']
            id = result['id']
            kb.add(
                types.InlineKeyboardButton(text=f'🎧 {artist} - {title}', callback_data=f'id|{str(id)}')
            )
            index.append('+')
        await msg.answer(f'<b>🔎Готово! Я нашёл {str(len(index))} вариантов\n'
                         f'Просто выбери один из и мы его скачаем!</>', reply_markup=kb)

    except:
        pass


@dp.callback_query_handler()
async def call(call: types.CallbackQuery):
    if 'id' in call.data:
        msg = call.message
        data = call.data

        id1 = data.split('|')[1]

        upc = str(requests.get(api_deezer_url + 'track/' + id1).json()['album']['id'])
        upc = str(requests.get(api_deezer_url + 'album/' + upc).json()['upc'])

        get_data = methods.by_upc(upc=upc)
        if get_data == {'error': {'type': 'DataException', 'message': 'no data', 'code': 800}}:
            await msg.edit_text('<i>Код ошибки: </> <code>800</>\n'
                                '<i>Ответ от сервера:</> <code>No data</>\n\n'
                                f'<i>Ваш запрос: </><code>upc:{upc}</>')
        else:
            get_data = json.dumps(get_data)
            get_data = json.loads(get_data)

            album_id = get_data['id']
            album_title = get_data['title']
            album_link = get_data['link']
            album_cover = get_data['cover_xl']
            label = get_data['label']
            number_of_playlist = get_data['nb_tracks']
            duration_mins = str(datetime.timedelta(seconds=int(get_data['duration']))).replace('0:', '')
            release_date = get_data['release_date']
            artists = ' & '.join([artist['name'] for artist in get_data['contributors']])

            # получаю треклист и форматирую его

            tracklist_kb = types.InlineKeyboardMarkup()
            get_tracklist = methods.get_tracklist(album_id)
            get_tracklist = json.dumps(get_tracklist)
            get_tracklist = json.loads(get_tracklist)
            for track in get_tracklist['data']:
                artist = track['artist']['name']
                title = track['title']
                isrc = track['isrc']

                tracklist_kb.add(
                    types.InlineKeyboardButton(text=f'{artist} - {title}', callback_data=f'{isrc}')
                )
            tracklist_kb.add(
                types.InlineKeyboardButton(text='Deezer', url=album_link)
            )

            # отправляю результат

            await msg.delete()

            await msg.answer_photo(
                photo=requests.get(album_cover).content,
                caption=f'<i>Название: <code>{artists} - {album_title}</>\n'
                        f'Количество треков: <code>{str(number_of_playlist)}</>\n'
                        f'Длительность: <code>{duration_mins} минут</>\n'
                        f'Лейбл: <code>{label}</>\n'
                        f'Дата релиза: <code>{str(release_date)}</></>',
                reply_markup=tracklist_kb
            )
    else:
        await call.answer('Скачивание...')
        song_info = requests.get(api_deezer_url + 'track/isrc:' + call.data).json()
        song_id = song_info['link']
        song_cover = song_info
        get = requests.get(api_deezer_url + 'album/' + str(song_info['album']['id'])).json()['title']
        name = get + ' ' + str(
            requests.get(api_deezer_url + 'album/' + str(song_info['album']['id'])).json()['upc'])
        if '&' in name:
            name = name.replace('&', '')
        if '?' in name:
            name = name.replace('?', '')
        download.download_trackdee(
            f"{song_id}",
            output="./",
            quality="MP3_128",
            recursive_quality=True,
            recursive_download=True,
            not_interface=True)
        name_file = (os.listdir(name)[0])
        file_bytes = open(name + '/' + name_file, 'rb').read()
        os.remove(name + '/' + name_file)
        os.rmdir(name)
        me = await bot.get_me()
        await call.message.answer_audio(file_bytes, thumb=requests.get(
            'https://api.deezer.com/album/' + str(song_info['album']['id']) + '/image').content,
                                        caption=f'<code>https://t.me/{me.username}/</>')
        await call.message.delete()


# code by t.me/nikprotect
# code by t.me/nikprotect
# code by t.me/nikprotect

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
