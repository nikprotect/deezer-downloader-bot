# deezer-downloader-bot

Бот может скачивать с Deezer используя UPC код и термины поиска.

Код написал: t.me/nikprotect

## Установка

```
git clone https://gitlab.com/nikprotect/deezer-downloader-bot
cd deezer-downloader-bot && pip install -r req.txt
python bot.py
```

## Информация

Получение токена:
```
t.me/@BotFather
```
Поучение arl:
```
https://www.deezer.com/ru/
```



